import { Component, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  options : BarcodeScannerOptions;
  encodText: string ="";
  encodeData: any ={};
  scannedData:any = {};
  myVar:any;
  checkcam : boolean;

  @ViewChild('video') videoElement: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  videoWidth = 0;
  videoHeight = 0;
  streamco ;



  constructor(public navCtrl: NavController, public scanner:BarcodeScanner,private renderer:Renderer2) {
    this.setupconstraint();
    this.myFunction();
    this.checkcam=false;
  }





  myFunction() {
    this.myVar = setInterval(()=>{this.alertFunc();}, 3000);
  }

  alertFunc() {
    console.log("active : " + this.streamco.active);
    if(this.checkcam == true)
    {
      console.log("checkcam true : " + this.checkcam);
      let btnclick = document.getElementById('encode');
      btnclick.click();
      //this.stopalltracks();
      //this.setupconstraint();
      if(this.streamco.active)
      {
        this.checkcam = false;
      }
      this.checkcam = false;

    }
    else{
      console.log("checkcam false : " + this.checkcam);

    }
  }


  scan(){
    this.stopalltracks();
    this.checkcam = false;
    this.options = {
      prompt: 'Scan your barcode'
    };
    this.scanner.scan().then((data)=>{
      this.scannedData = data;

      //this.stopStreamedVideo(this.videoElement);
      //this.setupconstraint();
      //this.setupconstraint();
      console.log("checkcam = true")
      this.checkcam = true;
      this.stopalltracks();
      console.log(this.checkcam);
    },(err)=> {
      console.log('Error' + err);
    })

    console.log("test scan");


  }
  encode(){
    /*this.scanner.encode(this.scanner.Encode.TEXT_TYPE,this.encodText).then((data)=>{
      this.encodeData = data;
    },(err)=> {
      console.log('Error' + err);
    })*/
    /*for (let pas = 0; pas < 5; pas++) {
      this.stopalltracks();
      this.setupconstraint();
    }
    this.stopalltracks();
    this.setupconstraint();*/
    this.setupconstraint();
    console.log(this.checkcam);
    /*if(this.checkcam == true)
    {

    }
    else{

    }*/
  }



  constraints = {
      //audio: false,
      //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
      video: true,

      facingMode:"environment",
      width:{ideal: 1920},
      height:{ideal:1080}
    };

    // config la variable constraints
    setupconstraint(){

      // demande de permission camera
      navigator.mediaDevices.getUserMedia(this.constraints);

      // verification des permissions video et de la disponibilité de devices
      if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
        // enumeration des appareils connectes pour filtrer
        navigator.mediaDevices.enumerateDevices().then((result)=>{
          var constraints;
          console.log(result);

         result.forEach(function(device){
           // filtrer pour garder les flux video
           if(device.kind.includes("videoinput")){
             // filtrer le label de la camera
             if(device.label.includes("Integrated")){
               constraints = {
                 audio: false,
                 //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                 // affecter le deviceid filtré
                 video: {deviceId: device.deviceId},

                 facingMode:"environment",
                 width:{ideal : 1920},
                 height:{ideal:1080}
               };


             }
           }

         })
         // on lance la connexion de la vidéo
         if(constraints){
           this.startCamera(constraints);
         }
         //this.startCamera(constraints);
        });

       } else {
           alert('Sorry, camera not available.');
       }
     }



    // function launch connection camera
    startCamera(cs) {
     if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
       // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
       navigator.mediaDevices.getUserMedia(cs).then(this.attachVideo.bind(this)).catch(
         (error) => {

           console.log(error);
         }
       );

      } else {
          alert('Sorry, camera not available.');
      }
    }

    handleError(error) {
      console.log('Error: ', error);
    }


    // function qui attache le flux video à la balise video
    attachVideo(stream) {
      this.streamco = stream;
      console.log(stream.active);

      this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
      this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
        this.videoHeight = this.videoElement.nativeElement.videoHeight;
        this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
    console.log(this.videoElement.nativeElement);
    }

    // function take snapshot and send mail
    capture() {
      this.setupconstraint();
      this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
      this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
      this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
      var url = this.canvas.nativeElement.toDataURL();
      console.log(url);
    }


    stopStreamedVideo(videoElem) {
      const tracks = this.videoElement.nativeElement.getTracks();

      tracks.forEach(function(track) {
        track.stop();
      });
      this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', null);
    }


    stopalltracks(){
      let vid = <HTMLVideoElement>document.getElementById('video');
      console.log(vid);
      vid.srcObject.getTracks().forEach(function(track) {
        track.stop();
      });
    }



}
