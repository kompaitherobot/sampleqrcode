﻿webpackJsonp([0],{

/***/ 109:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 109;

/***/ }),

/***/ 150:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 150;

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, scanner, renderer) {
        this.navCtrl = navCtrl;
        this.scanner = scanner;
        this.renderer = renderer;
        this.encodText = "";
        this.encodeData = {};
        this.scannedData = {};
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 }
        };
        this.setupconstraint();
        this.myFunction();
        this.checkcam = false;
    }
    HomePage.prototype.myFunction = function () {
        var _this = this;
        this.myVar = setInterval(function () { _this.alertFunc(); }, 3000);
    };
    HomePage.prototype.alertFunc = function () {
        console.log("active : " + this.streamco.active);
        if (this.checkcam == true) {
            console.log("checkcam true : " + this.checkcam);
            var btnclick = document.getElementById('encode');
            btnclick.click();
            //this.stopalltracks();
            //this.setupconstraint();
            if (this.streamco.active) {
                this.checkcam = false;
            }
            this.checkcam = false;
        }
        else {
            console.log("checkcam false : " + this.checkcam);
        }
    };
    HomePage.prototype.scan = function () {
        var _this = this;
        this.stopalltracks();
        this.checkcam = false;
        this.options = {
            prompt: 'Scan your barcode'
        };
        this.scanner.scan().then(function (data) {
            _this.scannedData = data;
            //this.stopStreamedVideo(this.videoElement);
            //this.setupconstraint();
            //this.setupconstraint();
            console.log("checkcam = true");
            _this.checkcam = true;
            _this.stopalltracks();
            console.log(_this.checkcam);
        }, function (err) {
            console.log('Error' + err);
        });
        console.log("test scan");
    };
    HomePage.prototype.encode = function () {
        /*this.scanner.encode(this.scanner.Encode.TEXT_TYPE,this.encodText).then((data)=>{
          this.encodeData = data;
        },(err)=> {
          console.log('Error' + err);
        })*/
        /*for (let pas = 0; pas < 5; pas++) {
          this.stopalltracks();
          this.setupconstraint();
        }
        this.stopalltracks();
        this.setupconstraint();*/
        this.setupconstraint();
        console.log(this.checkcam);
        /*if(this.checkcam == true)
        {
    
        }
        else{
    
        }*/
    };
    // config la variable constraints
    HomePage.prototype.setupconstraint = function () {
        var _this = this;
        // demande de permission camera
        navigator.mediaDevices.getUserMedia(this.constraints);
        // verification des permissions video et de la disponibilité de devices
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // enumeration des appareils connectes pour filtrer
            navigator.mediaDevices.enumerateDevices().then(function (result) {
                var constraints;
                console.log(result);
                result.forEach(function (device) {
                    // filtrer pour garder les flux video
                    if (device.kind.includes("videoinput")) {
                        // filtrer le label de la camera
                        if (device.label.includes("Integrated")) {
                            constraints = {
                                audio: false,
                                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                                // affecter le deviceid filtré
                                video: { deviceId: device.deviceId },
                                facingMode: "environment",
                                width: { ideal: 1920 },
                                height: { ideal: 1080 }
                            };
                        }
                    }
                });
                // on lance la connexion de la vidéo
                if (constraints) {
                    _this.startCamera(constraints);
                }
                //this.startCamera(constraints);
            });
        }
        else {
            alert('Sorry, camera not available.');
        }
    };
    // function launch connection camera
    HomePage.prototype.startCamera = function (cs) {
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
            navigator.mediaDevices.getUserMedia(cs).then(this.attachVideo.bind(this)).catch(function (error) {
                console.log(error);
            });
        }
        else {
            alert('Sorry, camera not available.');
        }
    };
    HomePage.prototype.handleError = function (error) {
        console.log('Error: ', error);
    };
    // function qui attache le flux video à la balise video
    HomePage.prototype.attachVideo = function (stream) {
        var _this = this;
        this.streamco = stream;
        console.log(stream.active);
        this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', stream);
        this.renderer.listen(this.videoElement.nativeElement, 'play', function (event) {
            _this.videoHeight = _this.videoElement.nativeElement.videoHeight;
            _this.videoWidth = _this.videoElement.nativeElement.videoWidth;
        });
        console.log(this.videoElement.nativeElement);
    };
    // function take snapshot and send mail
    HomePage.prototype.capture = function () {
        this.setupconstraint();
        this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
        this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
        this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 0, 0);
        var url = this.canvas.nativeElement.toDataURL();
        console.log(url);
    };
    HomePage.prototype.stopStreamedVideo = function (videoElem) {
        var tracks = this.videoElement.nativeElement.getTracks();
        tracks.forEach(function (track) {
            track.stop();
        });
        this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', null);
    };
    HomePage.prototype.stopalltracks = function () {
        var vid = document.getElementById('video');
        console.log(vid);
        vid.srcObject.getTracks().forEach(function (track) {
            track.stop();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('video'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('canvas'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "canvas", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\antho\komappweb\laetibaterry\QrCodeScan\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Ionic Blank\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="">\n    <ion-input type="text" [(ngModel)]="encodText"></ion-input>\n    <button ion-button (click)="encode()" id="encode" type="button" name="button"> Encode</button>\n  </div>\n  <div class="">\n    <button ion-button (click)="scan()" type="button" name="button"> Scan</button>\n    <div class="">\n      <label for="">Your barcode is:</label>\n      <br>\n      <span>{{scannedData.text}}</span>\n    </div>\n    <video #video autoplay id="video" class="video"></video>\n    <canvas #canvas style="display:none;"> </canvas>\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\antho\komappweb\laetibaterry\QrCodeScan\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(218);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(194);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(194);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\antho\komappweb\laetibaterry\QrCodeScan\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Users\antho\komappweb\laetibaterry\QrCodeScan\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[195]);
//# sourceMappingURL=main.js.map